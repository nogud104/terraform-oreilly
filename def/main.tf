terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
    profile    = "default"
}
# Add ec2 instance
resource "aws_instance" "example" {
    ami = "ami-08c40ec9ead489470"
    instance_type = "t2.micro"
    tags = { 
        Name = "terraform-example"
        }
}